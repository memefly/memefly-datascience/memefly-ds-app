from marshmallow import Schema, fields
from marshmallow import ValidationError

import typing as t
import json


class InvalidInputError(Exception):
    """Invalid model input."""

SYNTAX_ERROR_FIELD_MAP= {
    'meme_name': 'meme_name'
}


class MemeflyRequestSchema(Schema):
    meme_name = fields.Str(allow_none=False)

def validate_inputs(input_json):

    schema = MemeflyRequestSchema(strict=True, many=True)
    input_data = json.loads(input_json)

    try:
        schema.load(input_data)
    except ValidationError as e:
        errors = e.messages


