import pytest
from api.app import create_app
from api.config import TestingConfig
from starlette.testclient import TestClient


app = create_app(config_object=TestingConfig)
client = TestClient(app)


def test_root():
    response = client.get("")
    assert response.status_code == 200
    #assert response.json == "Memefly REST API, Model 2.2.2"

def test_get_meme_names():
    response = client.get("/get-meme-names")
    assert response.status_code == 200

def test_generate_meme_text():
    """
    in progress
    """
   # response = client.post(
   #         "/generate_meme_text",
   #         json={'meme_name': "y-u-no"
   #             }
   #         )
   # assert response.status_code == 200
