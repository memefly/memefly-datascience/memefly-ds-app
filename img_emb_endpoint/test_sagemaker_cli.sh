#!/usr/bin/env bash
ENDPOINT_NAME=img-emb-tfserving-v1

aws sagemaker-runtime invoke-endpoint \
--endpoint-name ${ENDPOINT_NAME} \
--body '{"instances": [1.0,2.0,5.0]}' response.json 
