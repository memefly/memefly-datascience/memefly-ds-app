#!/usr/bin/env bash
#export IS_DEBUG=${DEBUG:-false}
#exec uvicorn --host :${PORT:-8000} --access-log run:app
exec python run.py
