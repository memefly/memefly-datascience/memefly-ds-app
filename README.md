# Version Schema

VERSION Schema {api_version.model_version.data_version}

2019-12-03 - 3.3.2 API, release 3, refactored added Beam Search, updated beam width to 1

2019-11-23 - 2.2.2 API, refactored env to config. Added test cases.

2019-11-20 - 2.2.2 API release 2, now with url and bounding box, model v2, data v2

2019-11-13 - 0.2 Release, Refactored.

2019-11-08 - 0.1 Release                                                  

### Endpoints Usage

1. ROUTE: `/get-meme-names`
 
METHOD: `GET`
OUTPUT: 
```
[
    "Original-Stoner-Dog",
    "Musically-Oblivious-8th-Grader",
    "Hedonism-Bot",
    "Joe-Biden",
    "Gangnam-Style",
    "Scumbag-Boss",
    "Archer",
    "Lame-Pun-Coon",
    "Scumbag-Steve",
    "Maury-Lie-Detector",
    "Slowpoke",
    ...
]
```

2. ROUTE: `/generate-meme-text` 

METHOD: `POST`

INPUT: 
```
{
    "meme_name": "y-u-no"
}
```

OUTPUT: 
```
{
    "meme_text": "meme generator y u no let me delete mistakes"
}
```


# Package Directory Structure:
```
├── run.sh                <- Convenience script to run run.py
├── build_api_docker.sh   <- Convenience script to build docker image
├── run_api_docker.sh     <- Convenience script to run docker image
├── run.py                <- Run the API endpoint
├── api                   <- Memefly API endpoint package
│    ├── app.py
│    ├── endpoints.py     <- API routing endpoints
│    ├── config.py        <- Logging and config for the API
│    ├── validation.py    <- [In Progress] Schema validation.
│    └── pipeline         <- Submodule for ML pipeline
│          ├── ingesting.py
│          ├── preprocessing.py
│          └── inferencing.py
│
├── tests                 
├── logs                   
├── VERSION
├── Dockerfile           
├── requirements.txt
└── README.md
```
